# Pixel Shootout

Third person shooter made with the Orx open-source game engine

The project is called Orx Vice for... some reason (I forgot how I thought of this name). I did not write the Orx library and this project has no affiliation to the Orx project. To avoid confusion, the actual application name is Pixel Shootout.

This is my first attempt at using Orx. The project might never reach a deployable state, but let's stay optimistic shall we? (Optimism is something I do not have a lot of)

Project available under GPLv3. Orx library available under Zlib. See LICENSE for more details.

### Gameplay:
Use WASD to move around.
The player (in blue) faces the cursor; move the cursor to aim at opponents (in orange and red).
Click to fire (the frequency at which you can fire is determined by your weapon).
Run into items to pick them up (coming soon).

Don't get shot.
